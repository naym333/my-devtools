![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Example plain HTML site using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
# [Muse Vue Ant Design Dashboard](http://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud) [![Tweet](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&logo=twitter)](https://twitter.com/intent/tweet?url=https://www.creative-tim.com/product/muse-vue-ant-design-dashboard&text=Check%20Muse%20Vue%20Ant%20Design%20made%20by%20@CreativeTim%20#webdesign%20#dashboard%20#antdesign%20#vue%20https://www.creative-tim.com/product/muse-vue-ant-design-dashboard)

![version](https://img.shields.io/badge/version-1.0.0-blue.svg) [![GitHub issues open](https://img.shields.io/github/issues/creativetimofficial/muse-vue-ant-design-dashboard.svg)](https://github.com/creativetimofficial/muse-vue-ant-design-dashboard/issues?q=is%3Aopen+is%3Aissue) [![GitHub issues closed](https://img.shields.io/github/issues-closed-raw/creativetimofficial/muse-vue-ant-design-dashboard.svg)](https://github.com/creativetimofficial/muse-vue-ant-design-dashboard/issues?q=is%3Aissue+is%3Aclosed)

![Image](https://s3.amazonaws.com/creativetim_bucket/products/494/original/opt_md_ant_thumbnail.jpg)

Muse - Vue Ant Design Dashboard is a beautiful Ant Design Vue admin dashboard with a large number of components, designed to look beautiful and organized

Designed for those who like bold elements and beautiful websites. Made of hundred of elements, designed blocks and fully coded pages, Soft UI Dashboard is ready to help you create stunning websites and webapps.

We created many examples for pages like Sign In, Profile and so on. Just choose between a Basic Design, an illustration or a cover and you are good to go!

**Fully Coded Elements**

Muse - Vue Ant Design Dashboard is built with over 70 frontend individual elements, like buttons, inputs, navbars, navtabs, cards or alerts, giving you the freedom of choosing and combining. All components can take variations in colour, that you can easily modify using SASS files and classes. You will save a lot of time going from prototyping to full-functional code, because all elements are implemented.

View <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard/documentation/" target="_blank">all components here</a>.

**Documentation built by Developers**

Each element is well presented in a very complex documentation.
You can read more about the <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard/documentation/" target="_blank">documentation here</a>.

**Example Pages**

If you want to get inspiration or just show something directly to your clients,
you can jump start your development with our pre-built example pages. You will be able
to quickly set up the basic structure for your web project.
View <a href="https://www.creative-tim.com/muse-vue-ant-design-dashboard" target="_blank">example pages here</a>.

**HELPFUL LINKS**

- View <a href="https://github.com/creativetimofficial/muse-vue-ant-design-dashboard" target="_blank">Github Repository</a>

- Check <a href="https://www.creative-tim.com/faq" target="_blank">FAQ Page</a>

**Special thanks**

During the development of this dashboard, we have used many existing resources from awesome developers. We want to thank them for providing their tools open source:

- [Ant Design Vue](https://www.antdv.com/docs/vue/introduce/)- An enterprise-class UI design language for web applications

Let us know your thoughts below. And good luck with development!

## Table of Contents

* [Versions](#versions)
* [Demo](#demo)
* [Quick Start](#quick-start)
* [Documentation](#documentation)
* [File Structure](#file-structure)
* [Browser Support](#browser-support)
* [Resources](#resources)
* [Reporting Issues](#reporting-issues)
* [Technical Support or Questions](#technical-support-or-questions)
* [Licensing](#licensing)
* [Useful Links](#useful-links)

## Versions

[<img src="https://s3.amazonaws.com/creativetim_bucket/github/html.png" width="60" height="60" />](https://www.creative-tim.com/product/muse-vue-ant-design-dashboard?ref=readme-sud)

| HTML |
| --- |
| [![Muse Vue Ant Design Dashboard](https://s3.amazonaws.com/creativetim_bucket/products/494/thumb/opt_md_ant_thumbnail.jpg)](http://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud)

## Demo

- [Profile page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/profile?ref=readme-sud)
- [Sign in page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/sign-in?ref=readme-sud)
- [Sign up page](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/sign-up?ref=readme-sud)

[View More](http://demos.creative-tim.com/muse-vue-ant-design-dashboard).

## Quick start

Quick start options:

- Download from [Creative Tim](https://www.creative-tim.com/product/muse-vue-ant-design-dashboard?ref=readme-sud).

## Terminal Commands

1. Download and Install NodeJs from [NodeJs Official Page](https://nodejs.org/en/download/).
2. Navigate to the root / directory and run npm install to install our local dependencies.

## Documentation
The documentation for the Muse Vue Ant Design Dashboard is hosted at our [website](http://demos.creative-tim.com/muse-vue-ant-design-dashboard/documentation?ref=readme-sud).

### What's included

Within the download you'll find the following directories and files:

```
muse-vue-ant-design-dashboard
├── LICENSE
├── README.md
├── babel.config.js
├── gulpfile.js
├── package.json
├── public
│   ├── imagesd
│   └── index.html
├── src
│   ├── App.vue
│   ├── assets
│   ├── components
│   │   ├── Cards
│   │   │   ├── CardAuthorTable.vue
│   │   │   ├── CardBarChart.vue
│   │   │   ├── CardBillingInfo.vue
│   │   │   ├── CardConversations.vue
│   │   │   ├── CardCredit.vue
│   │   │   ├── CardInfo.vue
│   │   │   ├── CardInfo2.vue
│   │   │   ├── CardInvoices.vue
│   │   │   ├── CardLineChart.vue
│   │   │   ├── CardOrderHistory.vue
│   │   │   ├── CardPaymentMethods.vue
│   │   │   ├── CardPlatformSettings.vue
│   │   │   ├── CardProfileInformation.vue
│   │   │   ├── CardProject.vue
│   │   │   ├── CardProjectTable.vue
│   │   │   ├── CardProjectTable2.vue
│   │   │   └── CardTransactions.vue
│   │   ├── Charts
│   │   │   ├── ChartBar.vue
│   │   │   └── ChartLine.vue
│   │   ├── Footers
│   │   │   ├── DashboardFooter.vue
│   │   │   └── DefaultFooter.vue
│   │   ├── Headers
│   │   │   ├── DashboardHeader.vue
│   │   │   └── DefaultHeader.vue
│   │   ├── Sidebars
│   │   │   ├── DashboardSettingsDrawer.vue
│   │   │   └── DashboardSidebar.vue
│   │   └── Widgets
│   │       ├── WidgetCounter.vue
│   │       └── WidgetSalary.vue
│   ├── layouts
│   │   ├── Dashboard.vue
│   │   ├── DashboardRTL.vue
│   │   └── Default.vue
│   ├── main.js
│   ├── plugins
│   │   └── click-away.js
│   ├── router
│   │   └── index.js
│   ├── scss
│   │   ├── app.scss
│   │   ├── base
│   │   │   ├── _override.scss
│   │   │   ├── _typography.scss
│   │   │   ├── _utilities.scss
│   │   │   └── _variables.scss
│   │   ├── components
│   │   │   ├── _avatar.scss
│   │   │   ├── _badge.scss
│   │   │   ├── _button.scss
│   │   │   ├── _card.scss
│   │   │   ├── _chart.scss
│   │   │   ├── _dropdown.scss
│   │   │   ├── _list.scss
│   │   │   ├── _progress.scss
│   │   │   ├── _settings-drawer.scss
│   │   │   ├── _space.scss
│   │   │   ├── _table.scss
│   │   │   ├── _tag.scss
│   │   │   ├── _timeline.scss
│   │   │   └── _widget.scss
│   │   ├── form
│   │   │   ├── _checkbox.scss
│   │   │   └── _input.scss
│   │   ├── layouts
│   │   │   ├── _dashboard-rtl.scss
│   │   │   ├── _dashboard.scss
│   │   │   └── _default.scss
│   │   └── pages
│   │       ├── _profile.scss
│   │       ├── _sign-in.scss
│   │       └── _sign-up.scss
│   └── views
│       ├── 404.vue
│       ├── Billing.vue
│       ├── Dashboard.vue
│       ├── Layout.vue
│       ├── Profile.vue
│       ├── RTL.vue
│       ├── Sign-In.vue
│       ├── Sign-Up.vue
│       └── Tables.vue
└── vue.config.js
```

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/chrome.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/firefox.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/edge.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/safari.png" width="64" height="64"> <img src="https://s3.amazonaws.com/creativetim_bucket/github/browser/opera.png" width="64" height="64">

## Resources
- [Live Preview](https://demos.creative-tim.com/muse-vue-ant-design-dashboard?ref=readme-sud)
- [Download Page](https://www.creative-tim.com/product/muse-vue-ant-design-dashboard?ref=readme-sud)
- Documentation is [here](https://demos.creative-tim.com/muse-vue-ant-design-dashboard/documentation?ref=readme-sud)
- [License Agreement](https://www.creative-tim.com/license?ref=readme-sud)
- [Support](https://www.creative-tim.com/contact-us?ref=readme-sud)
- Issues: [Github Issues Page](https://github.com/creativetimofficial/muse-vue-ant-design-dashboard/issues)

## Reporting Issues
We use GitHub Issues as the official bug tracker for the Muse Vue Ant Design Dashboard. Here are some advices for our users that want to report an issue:

1. Make sure that you are using the latest version of the Muse Vue Ant Design Dashboard. Check the CHANGELOG from your dashboard on our [website](https://www.creative-tim.com/product/muse-vue-ant-design-dashboard?ref=readme-sud).
2. Providing us reproducible steps for the issue will shorten the time it takes for it to be fixed.
3. Some issues may be browser specific, so specifying in what browser you encountered the issue might help.

## Technical Support or Questions

If you have questions or need help integrating the product please [contact us](https://www.creative-tim.com/contact-us?ref=readme-sud) instead of opening an issue.

## Licensing

- Copyright 2021 [Creative Tim](https://www.creative-tim.com?ref=readme-sud)
- Creative Tim [license](https://www.creative-tim.com/license?ref=readme-sud)

## Useful Links

- [More products](https://www.creative-tim.com/templates?ref=readme-sud) from Creative Tim

- [Tutorials](https://www.youtube.com/channel/UCVyTG4sCw-rOvB9oHkzZD1w)

- [Freebies](https://www.creative-tim.com/bootstrap-themes/free?ref=readme-sud) from Creative Tim

- [Affiliate Program](https://www.creative-tim.com/affiliates/new?ref=readme-sud) (earn money)

##### Social Media

Twitter: <https://twitter.com/CreativeTim>

Facebook: <https://www.facebook.com/CreativeTim>

Dribbble: <https://dribbble.com/creativetim>

Instagram: <https://instagram.com/creativetimofficial>
