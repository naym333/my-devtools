const path = require('path');
const PrerenderSPAPlugin = require('prerender-spa-plugin');

const Renderer = PrerenderSPAPlugin.PuppeteerRenderer;

module.exports = {
	publicPath: process.env.NODE_ENV === 'production'
		? '/my-devtools/'
		: '/'
}

module.exports = {
	runtimeCompiler: true,

	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = 'Base64 encoder'
				return args
			})
	},
	configureWebpack: {
		plugins: [
			new PrerenderSPAPlugin({
				staticDir: path.join(__dirname, 'dist'),
				routes: [ '/', '/base64encode', '/base64encode' ],

				renderer: new Renderer({
					headless: true,
					renderAfterDocumentEvent: 'render-event'
				})
			})
		],
	},
}
